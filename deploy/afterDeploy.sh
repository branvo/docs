#!/bin/bash
if [ "$1" = "localhost" ]
then
    sudo mkdir -p tmp/apps/ && sudo chmod -R 2777 tmp/ && sudo rm -rf tmp/apps/*
    sudo touch tmp/apps/githash.json && sudo chmod 777 tmp/apps/githash.json && sudo echo '{"date":"'$(git log -1 --pretty="%ad")'","hash":"'$(git log -1 --pretty="%h")'"}' > tmp/apps/githash.json
    composer install

elif  [ "$1" = "develop" ]
then
    sudo mkdir -p tmp/apps/ && sudo chmod -R 2777 tmp/ && sudo rm -rf tmp/apps/*
    sudo touch tmp/apps/githash.json && sudo chmod 777 tmp/apps/githash.json && sudo echo '{"date":"'$(git log -1 --pretty="%ad")'","hash":"'$(git log -1 --pretty="%h")'"}' > tmp/apps/githash.json
    composer install
    ./vendor/bin/phpununit

elif  [ "$1" = "staging" ]
then
    sudo mkdir -p tmp/apps/ && sudo chmod -R 2777 tmp/ && sudo rm -rf tmp/apps/*
    sudo touch tmp/apps/githash.json && sudo chmod 777 tmp/apps/githash.json && sudo echo '{"date":"'$(git log -1 --pretty="%ad")'","hash":"'$(git log -1 --pretty="%h")'"}' > tmp/apps/githash.json
    composer install --no-dev
else
    if [ -d "/tmp/aws/docs" ]
    then
        sudo rsync -pr /tmp/aws/docs/ /var/www/"$DEPLOYMENT_GROUP_NAME"/
        sudo rm -rf /tmp/aws/docs
        cd /var/www/"$DEPLOYMENT_GROUP_NAME"
    fi
    composer install --no-dev
    curl -X POST -H 'Content-type: application/json' --data '{"text":"Deploy concluído: docs"}' https://hooks.slack.com/services/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
fi