.. Branvo Pay documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

##########
Consultas
##########

A BranvoPay possui uma API de consultas, ideal para realizar validações e útil em algumas etapas de cadastros de cartões, seja em carteiras virtuais ou em checkouts transparentes.
Veja abaixo os serviços disponíveis nessa API:

*****************
Consulta de BIN
*****************

:guilabel:`GET` ``https://api.branvopay.com/consulta/v2/bin?bin=xxxxxx``

A Consulta de BIN serve para obter informações sobre o cartão com base no seu BIN (6 primeiros dígitos). A API da BranvoPay consegue retornar, apenas com base no BIN,
a bandeira do cartão, o tipo do cartão (Crédito/Débito), a nacionalidade do cartão (nacional/internacional) e a natureza do cartão (corporativo/pessoal). Basta uma requisição
GET simples para o endpoit acima, informando o bin do cartão como `query param`. Não se esqueça de informar o seu token de autenticação no `header` da operação.

========
Exemplo
========

**Requisição**

``https://sandbox-api.branvopay.com/consulta/v2/bin?bin=555555``

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": {
           "brand": "mastercard",
           "type": "credit",
           "international": true,
           "corporative": false
      }
   }

**Campos do Retorno:**

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Campo**                  | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | brand                      | string          | Bandeira do cartão em `lowercase`                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | type                       | string          | Tipo do cartão: [credit, debit, multiple]                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | international              | boolean         | Cartão internacional?                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | corporative                | boolean         | Cartão corporativo (empresarial)?                          |
   +----------------------------+-----------------+------------------------------------------------------------+





*********************
Validação de Cartão
*********************

:guilabel:`POST` ``https://api.branvopay.com/consulta/v2/cartao``

A Validação de Cartão serve para validar um determinado cartão antes de processar uma compra. Essa opção pode ser utilizada antes de enviar a transação em si, para conferir
se o cartão é válido ou não. Não esqueça de enviar o seu token de autenticação no `header` da operação.

===========
Referência
===========

**Campos para requisição POST**

+----------------------------+-----------------+------------------------------------------------------------+
| **Campo**                  | **Tipo**        | **Descrição**                                              |
+============================+=================+============================================================+
| number                     | string          | Número do cartão                                           |
+----------------------------+-----------------+------------------------------------------------------------+
| date                       | string          | Date de validade do cartão no formato MM/AAAA              |
+----------------------------+-----------------+------------------------------------------------------------+
| code                       | string          | Código de segurança do cartão                              |
+----------------------------+-----------------+------------------------------------------------------------+

.. important::
   Necessário enviar uma requisição em JSON. Portanto, o `header` ``Content-type`` deve ser ``application/json``

========
Exemplo
========

**Requisição**

``https://sandbox-api.branvopay.com/consulta/v2/cartao``

.. code:: javascript

   {
      "number": "5544554455445544",
      "date": "12/2030",
      "code": "123",
   }

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "valid": true
      }
   }

.. note::
   Esse endpoint retorna apenas a informação booleana de se o cartão informado é valido ou não. Para obter informações sobre o cartão, utilize a Consulta de BIN, descrita acima.