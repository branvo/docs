***************************
Operação dos Testes na API
***************************

Para transacionar na API da Branvo Pay em ambiente sandbox, você precisa utilizar o seu token de testes. Para obtenção dos seus tokens de sandbox e de produção, você deve enviar um e-mail para <suporte@branvo.com>, com o assunto **Solicitação de Token**, informando, no corpo do e-mail, o CPF ou CNPJ da conta e o ambiente desejado.

.. note::
    Caso você ainda não possua um usuário para acesso ao painel, envie um e-mail para <suporte@branvo.com> solicitando o seu acesso.

Para visualizar as transações realizadas com o token de testes, você precisa ativar a opção "Ativar Sandbox" no painel da BranvoPay, no menu Configurações, conforme a imagem abaixo. Essa opção ativa o ambiente sandbox da plataforma, onde ela passa a operar apenas com o seu token de testes em ambiente sandbox.

.. image:: conf.png

.. important::
   Para envio de requisições para o ambiente sandbox da API <https://sandbox-api.branvopay.com>, deve ser utilizado obrigatoriamente o token de testes. O token de produção será inválido neste ambiente, sendo válido apenas no ambiente de produção da API.
