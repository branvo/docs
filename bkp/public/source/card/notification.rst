.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   
##################
URL de Notificação
##################

A URL de notificação da transação é o endereço através do qual a API da
BranvoPay notificará a aplicação do usuário sobre atualizações nas
transações.

A aplicação do usuário deverá manter a URL de notificação preparada para
receber via GET os seguintes parâmetros:

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | token                      | string          | | Token da conta da transação                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | code                       | string          | | Código da transação na plataforma, retornado pela API no |
   |                            |                 | | momento da sua criação                                   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | orderNumber                | string          | | Número do pedido enviado pelo usuário BranvoPay          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | status                     | int             | | Status da transação                                      |
   |                            |                 | | Consultar tabela de status                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | amount                     | decimal         | | Valor da transação                                       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | statusDescription          | string          | | Descrição do Status                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | dataProcessamento          | string          | | Data do processamento da transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

Este envio será disparado automaticamente pela API assim que houver
atualizações em uma transação (captura, cancelamento).


Para realizar os testes, recomendamos o uso da ferramenta POSTMAN ou do
próprio browser, enviando os parâmetros acima via GET para a sua URL de
notificação.