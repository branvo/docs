.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   
################
Tabela de status
################

.. table::
   :widths: auto

   +--------+------------------------+---------------------------------------------------------------------+
   | Código | Status                 | Descrição                                                           |
   +========+========================+=====================================================================+
   | 0      | **PENDENTE**           | | Boleto está pendente de registro                                  |
   +--------+------------------------+---------------------------------------------------------------------+
   | 1      | **LIQUIDADO**          | | Boleto está liquidado (pago)                                      |
   +--------+------------------------+---------------------------------------------------------------------+
   | 2      | **BAIXADO**            | | Boleto está baixado (cancelado)                                   |
   +--------+------------------------+---------------------------------------------------------------------+
   | 3      | **ENTRADA CONFIRMADA** | | Boleto foi registrado                                             |
   |        |                        | | Ver este termo no glossário para mais detalhes                    |
   +--------+------------------------+---------------------------------------------------------------------+
   | 4      | **ALTERACAO**          | Boleto está pendente de alguma alteração                            |
   +--------+------------------------+---------------------------------------------------------------------+
   | 5      | **PROTESTADO**         | | Boleto foi enviado para protesto                                  |
   +--------+------------------------+---------------------------------------------------------------------+
   | 6      | **OUTROS**             | | Outras ocorrências.                                               |
   |        |                        | | Entrar em contato com o suporte técnico                           |
   +--------+------------------------+---------------------------------------------------------------------+
   | 9      | **ERRO**               | | Erro no registro do boleto                                        |
   |        |                        | | Entrar em contato com o suporte técnico                           |
   +--------+------------------------+---------------------------------------------------------------------+