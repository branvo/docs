****************************
Códigos de Retorno de Erros
****************************

Os códigos de erros da API Branvo Pay se dividem em 3 tipos. ARG, EPT e LGH, organizados conforme a tabela abaixo. Os códigos abaixo são os informados no parâmetro "errorCode" do retorno JSON.

=================== =============================================================================================
Tipo                Descrição                                                                                    
=================== =============================================================================================
ARG                 Parâmetro inválido
EPT                 Parâmetro não recebido
LGH                 Parâmetro excede o limite de caracteres
=================== =============================================================================================

.. important::
   Os retornos que envolvem a operação cartao/new, devem ser considerados também nas operações cartao/split e cartao/recurrent.

=================== ======================= ============================================================================================= ==============================================
Código              Parâmetro               Descrição                                                                                     Operações
=================== ======================= ============================================================================================= ==============================================
ARG001               state                  Estado do endereço do cliente inválido.                                                       cartao/new, bankslip/emission
ARG002               postcode               CEP do endereço do cliente inválido.                                                          cartao/new, bankslip/emission
ARG003               responsible            Responsável do endereço do cliente inválido.                                                  cartao/new, bankslip/emission
ARG004               abbreviate             Parâmetro abbreviate inválido. Deve ser 0 ou 1.                                               cartao/new, bankslip/emission
ARG005               ipAddress              Endereço IP da transação inválido.                                                            cartao/new
ARG006               amount                 Valor do boleto inválido. O valor mínimo é R$ 3,00.                                           bankslip/emission  
ARG007               dueDate                .. line-block:: 
                                               A data de vencimento do boleto é inválida.
                                               Deve ser no formato aaaa-mm-dd                                                             bankslip/emission
ARG008               dueDate                .. line-block::                                                                               bankslip/emission
                                               A data de vencimento do boleto deve ser a partir
                                               de 2 dias da geração.                        
ARG009               installments           Número de parcelas inválido. Deve ser entre 1 e 24.                                           bankslip/emission
ARG010               maxOverdueDays         Prazo pós vencimento inválido. Deve ser entre 0 e 30.                                         bankslip/emission
ARG011               fine                   O valor da multa não pode exceder 2% do valor do boleto.                                      bankslip/emission
ARG012               interest               O valor dos juros deve ficar entre 0 e 1.                                                     bankslip/emission
ARG013               name                   Informe nome completo do cliente, com nome e sobrenome.                                       cartao/new, bankslip/emission
ARG014               type                   Opção de tipo de cliente inválida. Deve ser PF ou PJ.                                         cartao/new, bankslip/emission
ARG015               document               CPF do cliente inválido.                                                                      cartao/new, bankslip/emission
ARG016               document               CNPJ do cliente inválido.                                                                     cartao/new, bankslip/emission
ARG017               mail                   E-mail do cliente inválido.                                                                   cartao/new, bankslip/emission
ARG018               secondaryMail          E-mail secundário do cliente inválido.                                                        bankslip/emission
ARG019               phone                  Número do telefone do cliente inválido.                                                       cartao/new, bankslip/emission
ARG020               birthday               .. line-block::                                                                               cartao/new, bankslip/emission
                                               Data de nascimento do cliente inválida.                                                    
                                               Deve ser no formato aaaa-mm-dd                        
ARG021               cardNumber             Cartão não aceito na plataforma.                                                              cartao/new
ARG022               cardNumber             Comprimento do número do cartão inválido para esta bandeira.                                  cartao/new
ARG023               cardNumber             Número do cartão inválido.                                                                    cartao/new
ARG024               cardNumber             Combinação inválida de bandeira e número do cartão.                                           cartao/new
ARG025               cardDate               Cartão vencido.                                                                               cartao/new
ARG026               cardDate               Cartão vencido.                                                                               cartao/new
ARG027               cardCode               Código de segurança do cartão inválido.                                                       cartao/new
ARG028               antiFraud              Parâmetro antiFraud inválido. Deve ser 0 ou 1.                                                cartao/new
ARG029               value                  Parâmetro value inválido. Deve ser positivo e no formato 0.00.                                cartao/new
ARG030               parcelNumber           Parâmetro parcelNumber inválido. Deve ser entre 1 e 12.                                       cartao/new
ARG031               capture                Parâmetro capture inválido. Deve ser 0 ou 1.                                                  cartao/new
ARG032               split                  Parâmetro split inválido. Deve ser 0 ou 1.                                                    bankslip/emission
ARG033               splitType              Parâmetro splitType inválido. Deve ser "val" ou "perc".                                       cartao/split, bankslip/emission
ARG034               splitAmount            Parâmetro splitAmount inválido. Deve ser um array de valores.                                 cartao/split, bankslip/emission
ARG035               splitToken             Parâmetro splitToken inválido. Deve ser um array de tokens.                                   cartao/split, bankslip/emission
ARG036               splitToken             .. line-block::                                                                               cartao/split, bankslip/emission
                                               Parâmetros splitToken e splitAmount devem possuir
                                               o mesmo comprimento.                         
ARG037               dueDate                A data de vencimento do boleto inválida.                                                      bankslip/emission   
EPT001               street                 Logradouro do endereço do cliente não informado.                                              cartao/new, bankslip/emission
EPT002               number                 Número do endereço do cliente não informado.                                                  cartao/new, bankslip/emission
EPT003               neighbourhood          Bairro do endereço do cliente não informado.                                                  cartao/new, bankslip/emission
EPT004               city                   Cidade do endereço do cliente não informada.                                                  cartao/new, bankslip/emission
EPT005               state                  Estado do endereço do cliente não informado.                                                  cartao/new, bankslip/emission
EPT006               postcode               CEP do endereço do cliente não informado.                                                     cartao/new, bankslip/emission
EPT007               description            Parâmetro description não informado.                                                          bankslip/emission
EPT008               reference              Parâmetro reference não informado.                                                            bankslip/emission
EPT009               amount                 Parâmetro amount não informado.                                                               bankslip/emission
EPT010               dueDate                Parâmetro dueDate não informado.                                                              bankslip/emission
EPT011               installments           Parâmetro installments não informado.                                                         bankslip/emission
EPT012               name                   Nome do cliente não informado.                                                                bankslip/emission
EPT013               type                   Tipo de cliente não informado.                                                                cartao/new, bankslip/emission
EPT014               document               Documento (CPF) do cliente não informado.                                                     cartao/new, bankslip/emission
EPT015               document               Documento (CNPJ) do cliente não informado.                                                    cartao/new, bankslip/emission
EPT016               mail                   E-mail do cliente não informado.                                                              cartao/new, bankslip/emission
EPT017               phone                  Telefone do cliente não informado.                                                            cartao/new, bankslip/emission
EPT018               cardNumber             Número do cartão não informado.                                                               cartao/new
EPT019               cardDate               Data de vencimento do cartão não informada.                                                   cartao/new
EPT020               cardDate               Data de vencimento do cartão não informada.                                                   cartao/new
EPT021               cardCode               Código de segurança do cartão não informado.                                                  cartao/new
EPT022               orderNumber            Parâmetro orderNumber não informado.                                                          cartao/new
EPT023               value                  Parâmetro value não informado.                                                                cartao/new
EPT024               parcelNumber           Parâmetro parcelNumber não informado.                                                         cartao/new
EPT025               splitType              Parâmetro splitType não informado.                                                            cartao/split, bankslip/emission 
EPT026               splitAmount            Parâmetro splitAmount não informado.                                                          cartao/split, bankslip/emission
EPT027               splitToken             Parâmetro splitToken não informado.                                                           cartao/split, bankslip/emission
EPT028               cardNumber             Número do cartão não informado.                                                               cartao/new
LGH001               street                 O endereço deve possuir no máx. 40 caracteres                                                 cartao/new, bankslip/emission
LGH002               number                 O número deve possuir no máximo 40 caracteres.                                                cartao/new, bankslip/emission
LGH003               neighbourhood          O bairro deve possuir no máximo 20 caracteres.                                                cartao/new, bankslip/emission
LGH004               city                   A cidade deve possuir no máximo 20 caracteres.                                                cartao/new, bankslip/emission
LGH005               complement             O complemento deve possuir no máximo 30 caracteres.                                           cartao/new, bankslip/emission
LGH006               responsible            A responsável deve possuir no máximo 40 caracteres.                                           cartao/new, bankslip/emission
LGH007               browser                O browser do cliente deve possuir no máximo 30 caracteres.                                    cartao/new
LGH008               os                     .. line-block                                                                                 cartao/new
                                               O sistema operacional do cliente deve possuir
                                               no máximo 30 caracteres.                        
LGH009               description            Parâmetro description deve possuir no máximo 400 caracteres.                                  bankslip/emission
LGH010               reference              Parâmetro reference deve possuir no máximo 10 caracteres.                                     bankslip/emission
LGH011               name                   O nome deve possuir no máximo 40 caracteres.                                                  cartao/new, bankslip/emission
=================== ======================= ============================================================================================= ==============================================