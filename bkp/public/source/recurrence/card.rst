.. Branvo Pay documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

################################
Recorrência - Cartão de Crédito
################################

*******************************************
Criar uma Recorrência por Cartão de Crédito
*******************************************

:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/recurrent``

**Endpoint utilizado para criar uma Transação Recorrente**

Uma transação recorrente é uma cobrança que será feita automaticamente pela Branvo Pay, no período informado pelo cliente. É o método utilizado por clubes de assinatura
e assinatura de serviços online que possuem pagamento mensal/anual.

==========
Requisição
==========

.. important::
   A requisição para Recorrência requer o envio de todos os parâmetros de uma nova transação comum (new), juntamente com os parâmetros da tabela abaixo:

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | frequence*                 | string          | | Frequência de cobrança                                   | 
   |                            |                 | |                                                          |
   |                            |                 | | Valores aceitos:                                         |
   |                            |                 | | [monthly (mensal), bimonthly (cada 2 meses),             |
   |                            |                 | | quarterly (trimestral), semester (semestral),            |
   |                            |                 | | yearly (anual)]                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | dayNumber                  | string          | | Dia do mês para execução dessa cobrança                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | finalDate                  | string          | | Data final da recorrência. Ela será executada            |
   |                            |                 | | automaticamente até esta data.                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | startDate                  | string          | | Data de início da recorrência. Ela será executada        |
   |                            |                 | | automaticamente a partir desta data e, após, sempre      |
   |                            |                 | | no dia do mês informado em dayNumber, e, caso não        |
   |                            |                 | | informado, será sempre no mesmo dia presente neste campo.|
   |                            |                 | | Quando a data de início não for informada, a recorrência |
   |                            |                 | | será iniciada na data atual.                             |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   Os dados de transação enviados nesta requisição servirão como base para as próximas cobranças. É possível atualizar os dados da recorrência, como explicado posteriormente
   nesta documentação. Esta transação pode ser capturada automaticamente ou não, conforme parâmetro 'capture' recebido. Fica sob responsabilidade do cliente Branvo Pay capturar
   esta transação posteriormente. As próximas transações da recorrência sempre serão capturadas automaticamente.

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": [
         {
            "code": int,
            "accountName": string,
            "payerName": string,
            "orderNumber": string,
            "status": int,
            "amount": decimal,
            "fraudPoints": int,
            "dateAnalysis": string,
            "message": string,
            "recurrence": {
                  "active": bool,
                  "amount": decimal,
                  "name": string,
                  "cardDate": string,
                  "cardNumber": string,
                  "code": int,
                  "createdAt": string,
                  "lastCharge": string,
                  "nextCharge": string,
                  "finalDate": string,
                  "frequence": string
            }
         }
      ]
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token*              \********************************
   **Body**                    **Valor enviado**
   clientName*                 nome completo
   clientType*                 PF
   clientDocument*             000\.000\.000-00
   clientMail*                 contato@exemplo.com
   clientPhone*                \(00\) 0000-0000
   clientSecondaryMail         contato2@exemplo.com
   clientBirthDate             0000-00-00
   billingStreet*              rua
   billingNumber*              00
   billingNeighbourhood*       bairro
   billingCity*                cidade
   billingState*               sigla do estado
   billingPostcode*            00000-000
   billingComplement           complemento
   billingResponsible*         nome completo
   deliveryStreet*             rua
   deliveryNumber*             00
   deliveryNeighbourhood*      bairro
   deliveryCity*               cidade
   deliveryState*              sigla do estado
   deliveryPostcode*           00000-000
   deliveryComplement          complemento
   deliveryResponsible*        nome completo
   cardOwnerName*              nome completo
   cardOwnerType*              PJ
   cardOwnerDocument*          00\.000\.000/0000-00
   cardOwnerPhone*             \(00\) 00000-0000
   cardFlag*                   bandeira
   cardNumber*                 0000\.0000\.0000\.0000
   cardMonth*                  00
   cardYear*                   0000
   cardCode*                   000
   antiFraud*                  1
   orderNumber*                11111
   value*                      50.00
   parcelNumber*               10
   capture*                    1
   ipAddress                   192.168.0.0
   os                          sistema operacional
   notificationUrl             url.exemplo.com
   browser                     navegador
   frequence                   monthly
   finalDate                   2020-01-01
   =========================== =================================

Header:

.. code:: javascript

   {
      "Account-Token":"*********************"
   }

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": [
         {
            "code": 123,
            "accountName": "CONTA DA TRANSAÇÃO",
            "payerName": "NOME COMPLETO",
            "orderNumber": "11111",
            "status": 41,
            "amount": "50.00",
            "fraudPoints": 0,
            "dateAnalysis": "06/11/2018 10:21:23",
            "message": "Transação realizada com sucesso",
            "recurrence": {
                  "active": true, //se a recorrência está ativa ou não
                  "amount": "50.00",
                  "cardDate": "00/0000",
                  "cardNumber": "000000******0000",
                  "name": "NOME COMPLETO",
                  "code": 86, //código da recorrência, utilizado em requisições posteriores (importante salvar este código)
                  "createdAt": "06/12/2018 10:21:19", //data da criação da recorrência
                  "lastCharge": "06/12/2018 10:21:19", //data da execução da última cobrança realizada
                  "nextCharge": "01/01/2020", //data a próxima cobrança
                  "finalDate": "01/01/2020", //data final da recorrêcia
                  "frequence": "monthly"
            }
         }
      ]
   }


*********************************
Atualizar uma Recorrência
*********************************


:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/recurrent/{recurrenceCode}``

**Endpoint utilizado para atualizar uma Recorrência**

.. important::
   Os dados atualizados nesta requisição são os da recorrência, ou seja, as transações já efetuadas não serão afetadas. As próximas transações passarão a ser realizadas
   com estes dados. Através deste método, é possível estender a validade de uma recorrência, pausar uma recorrência, alterar a sua frequência e o seu valor.

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Header**                 | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | Account-Token*             | string          | | Token da conta na BranvoPay                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | frequence                  | string          | | Frequência de cobrança                                   | 
   |                            |                 | |                                                          |
   |                            |                 | | Valores aceitos:                                         |
   |                            |                 | | [monthly (mensal), bimonthly (cada 2 meses),             |
   |                            |                 | | quarterly (trimestral), semester (semestral),            |
   |                            |                 | | yearly (anual)]                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | amount                     | decimal         | | Valor da recorrência.                                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | finalDate                  | string          | | Data final da recorrência. Ela será executada            |
   |                            |                 | | automaticamente até esta data.                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | active                     | int             | | Recorrência ativa ou desativada/pausada                  |
   |                            |                 | | Valores aceitos: [0 (ativa), 1 (desativada)]             |
   +----------------------------+-----------------+------------------------------------------------------------+
   
.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   Nenhum dos parâmetros do corpo da requisição é obrigatório, porém é necessário informar pelo menos um deles para atualização.

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": {
         "active": bool,
         "amount": decimal,
         "name": string,
         "cardDate": string,
         "cardNumber": string,
         "code": int,
         "createdAt": string,
         "lastCharge": string,
         "nextCharge": string,
         "finalDate": string,
         "frequence": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   **Body**                    **Valor enviado**
   frequence                   yearly
   finalDate                   2021-01-01
   amount                      110.00
   active                      0
   =========================== =================================

Header:

.. code:: javascript

   {
      "Account-Token":"*********************"
   }

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "active": false, //se a recorrência está ativa ou não
         "amount": "110.00",
         "name": "NOME COMPLETO",
         "cardDate": "00/0000",
         "cardNumber": "000000******0000",
         "code": 86, // código da recorrência, usado para atualização e consulta
         "createdAt": "06/12/2018 10:21:19", //data da criação da recorrência
         "lastCharge": "06/12/2018 10:21:19", //data da execução da última cobrança da recorrência
         "nextCharge": "01/01/2021", //data da próxima cobrança
         "finalDate": "01/01/2021", //data final da recorrêcia
         "frequence": "yearly"
      }
   }

.. note::
   Este método não permite atualização dos dados de cartão de crédito utilizados para a cobrança recorrente. Para isso, é necessário desativar a recorrência e
   criar uma nova.


*********************************
Consultar uma Recorrência
*********************************

:guilabel:`GET` ``https://api.branvopay.com/cartao/v2/recurrent/{recurrenceCode}``

**Endpoint utilizado para consultar os dados de uma Recorrência**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": {
         "active": bool,
         "amount": decimal,
         "name": string,
         "cardDate": string,
         "cardNumber": string,
         "code": int,
         "createdAt": string,
         "lastCharge": string,
         "nextCharge": string,
         "finalDate": string,
         "frequence": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/recurrent/86

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "active": false, //se a recorrência está ativa ou não
         "amount": "110.00",
         "name": "NOME COMPLETO",
         "cardDate": "00/0000",
         "cardNumber": "000000******0000",
         "code": 86,
         "createdAt": "06/12/2018 10:21:19", //data da criação da recorrência
         "lastCharge": "06/12/2018 10:21:19", //data da execução da última transação da recorrência
         "nextCharge": "01/01/2021", //data final da recorrêcia
         "finalCate": "01/01/2021", //data final da recorrêcia
         "frequence": "yearly"
      }
   }