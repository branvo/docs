.. Branvo Pay documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

####################################
Introdução às Cobranças Recorrentes
####################################

As cobranças recorrentes são cobranças feitas automaticamente pela BranvoPay, na periodicidade que for configurada, que pode ser mensal, bimestral, trimestral, semestral e anual.
Atualmente, é possível criar cobranças recorrentes por boleto e cartão de crédito. É um serviço útil para quem cobra mensalidades ou assinaturas periódicas.

Via API, é possível configurar a data de início das cobranças, a data final das cobranças, o dia do mês para realização das cobranças, bem como alterar o valor da cobrança,
ativar/desativar as cobranças a qualquer momento e também alterar a sua periodicidade, se necessário.

Em nosso sistema, para cada cobrança, é feito um registro de recorrência único, de forma que é possível alterar as informações de cada cobrança separadamente.

A cada dia, caso existam recorrências executadas em sua conta, é enviado um e-mail de notificação, com o relatório de cobranças feitas por cartão e por boleto. 

A seguir, exemplos de uso da API de recorrência da BranvoPay.

*********************************
Exemplo prático de uso da API 1
*********************************

Se for cadastrada uma recorrência mensal para iniciar no dia 10/05, porém configurada para ser executada no dia 15 de cada mês, a primeira cobrança será feita no dia 10/05, e as cobranças seguintes
serão feitas no dia 15 de cada mês, ou seja, 15/06, 15/07, 15/08 e assim por diante.

Caso for cadastrada uma recorrência sem data inicial, porém configurada para ser executada no dia 10 de cada mês, a primeira cobrança será feita imediatamente, e as cobranças seguintes serão
feitas no dia 10 de cada mês.

Caso for cadastrada uma recorrência sem data inicial e sem dia do mês configurado, a primeira cobrança será realizada imediatamente e as cobranças seguintes no mesmo dia 
dos meses subsequentes. Ex.: Se hoje for dia 12/05, e for criado um registro recorrente, a primeira cobrança será neste dia, e as seguintes no dia 12/06, 12/07, 12/08, e 
assim por diante.

*********************************
Exemplo prático de uso da API 2
*********************************

Um clube de assinaturas utiliza a API da BranvoPay para gerenciar suas cobranças recorrentes de mensalidade. A cobrança é feita por cartão e boleto.
O clube possui 2 planos: um de R$ 19,90 e outro de R$ 99,90.

A empresa irá criar uma recorrência para cada membro do clube. Quando for necessário pausar ou desativar a cobrança de algum membro, a empresa utiliza a API para a atualização
do registro.

Caso o clube queira dar desconto no valor para algum membro específico por algum período, a empresa utiliza a API para realizar a atualização dos valores das recorrências
dos membros respectivos.

Caso o clube mude os valores dos planos para R$ 29,90 e R$ 119,90, a empresa pode ou não atualizar os planos dos membros atuais para correção dos valores, ou pode apenas 
alterar às requisições para a API da BranvoPay dos novos membros. 

Desta forma, deixamos o mais flexível possível para que a empresa gerencie cada uma das cobranças de forma única e altere as informações de cada membro do clube conforme
o necessário.

A seguir, as especificações para integração para cobranças recorrentes via cartão de crédito e boleto.
