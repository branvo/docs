.. Branvo Pay documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

######################
Recorrência - Boleto
######################

*******************************************
Criar uma Recorrência por Boleto Bancário
*******************************************

:guilabel:`POST` ``https://api.branvopay.com/boleto/v2/emission``

**Endpoint utilizado para criar um Boleto Recorrente**

.. note::
   O endpoint para criação de uma cobrança recorrente por boleto é o mesmo da emissão de boletos comum, porém com alguns parâmetros adicionais, como especificado abaixo.


Uma transação recorrente é uma cobrança que será feita automaticamente pela Branvo Pay, no período informado pelo cliente. É o método utilizado por clubes de assinatura
e assinatura de serviços online que possuem pagamento mensal/anual.

==========
Requisição
==========

.. important::
   A requisição para um boleto recorrence requer o envio de todos os parâmetros de uma emissão de boleto comum (emission), juntamente com os parâmetros da tabela abaixo:

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | recurrent*                 | int             | | Para boleto recorrente, enviar 1                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | frequence*                 | string          | | Frequência de cobrança                                   | 
   |                            |                 | |                                                          |
   |                            |                 | | Valores aceitos:                                         |
   |                            |                 | | [monthly (mensal), bimonthly (cada 2 meses),             |
   |                            |                 | | quarterly (trimestral), semester (semestral),            |
   |                            |                 | | yearly (anual)]                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | dayNumber                  | string          | | Dia do mês para execução dessa cobrança                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | finalDate                  | string          | | Data final da recorrência. Ela será executada            |
   |                            |                 | | automaticamente até esta data.                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | startDate                  | string          | | Data de início da recorrência. Ela será executada        |
   |                            |                 | | automaticamente a partir desta data e, após, sempre      |
   |                            |                 | | no dia do mês informado em dayNumber, e, caso não        |
   |                            |                 | | informado, será sempre no mesmo dia presente neste campo.|
   |                            |                 | | Quando a data de início não for informada, a recorrência |
   |                            |                 | | será iniciada na data atual.                             |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   Os dados do boleto enviados nesta requisição servirão como base para as próximas cobranças. É possível atualizar os dados da recorrência, como explicado posteriormente
   nesta documentação.

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data":{
         "charges":[
            {
               "code": string,
               "reference": string,
               "amount": decimal,
               "dueDate": string,
               "link": string,
               "installmentLink": string,
               "recurrence": {
                  "code": int,
                  "frequence": string,
                  "dayNumber": string,
                  "nextCharge": string,
               }
            }
         ]
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   +----------------------------+-----------------------------------------------------------------------------+
   | **Header**                 |                                                                             |
   +----------------------------+-----------------------------------------------------------------------------+
   | Account-Token*             | \*******************                                                        |
   +----------------------------+-----------------------------------------------------------------------------+
   | **Body**                   |                                                                             |
   +----------------------------+-----------------------------------------------------------------------------+
   | clientName*                | nome completo                                                               |
   +----------------------------+-----------------------------------------------------------------------------+
   | clientDocument*            | 000.000.000-00                                                              |
   +----------------------------+-----------------------------------------------------------------------------+
   | clientMail*                | contato@exemplo.com                                                         |
   +----------------------------+-----------------------------------------------------------------------------+
   | clientSecondaryMail        | contato2@exemplo.com                                                        |
   +----------------------------+-----------------------------------------------------------------------------+
   | clientPhone*               | 00 0000-0000                                                                |
   +----------------------------+-----------------------------------------------------------------------------+
   | clientBirthDate            | 0000-00-00                                                                  |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingStreet*             | Rua                                                                         |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingNumber*             | 00                                                                          |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingComplement          | complemento                                                                 |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingNeighborhood*       | bairro                                                                      |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingCity*               | cidade                                                                      |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingState*              | sigla do estado                                                             |
   +----------------------------+-----------------------------------------------------------------------------+
   | billingPostcode*           | 00000-000                                                                   |
   +----------------------------+-----------------------------------------------------------------------------+
   | description*               | descrição da compra                                                         |
   +----------------------------+-----------------------------------------------------------------------------+
   | reference*                 | 11111                                                                       |
   +----------------------------+-----------------------------------------------------------------------------+
   | amount*                    | 50.00                                                                       |
   +----------------------------+-----------------------------------------------------------------------------+
   | dueDate*                   | 0000-00-00                                                                  |
   +----------------------------+-----------------------------------------------------------------------------+
   | installments*              | 1                                                                           |
   +----------------------------+-----------------------------------------------------------------------------+
   | maxOverdueDays             | 5                                                                           |
   +----------------------------+-----------------------------------------------------------------------------+
   | fine                       | 1.99                                                                        |
   +----------------------------+-----------------------------------------------------------------------------+
   | interest                   | 0.99                                                                        |
   +----------------------------+-----------------------------------------------------------------------------+
   | notificationUrl            | www.url.exemplo.com                                                         |
   +----------------------------+-----------------------------------------------------------------------------+
   | recurrent                  | 1                                                                           |
   +----------------------------+-----------------------------------------------------------------------------+
   | frequence                  | monthly                                                                     |
   +----------------------------+-----------------------------------------------------------------------------+
   | dayNumber                  | 15                                                                          |
   +----------------------------+-----------------------------------------------------------------------------+


**Resposta**

.. code:: javascript

  {
      "success": true,
      "data": {
          "charges": [
              {
                  "code": "10000000000",
                  "reference": "11111",
                  "amount": "50.00",
                  "dueDate": "00/00/0000",
                  "link": "https://sandbox-api.branvopay.com/boleto/v2/printall/2aef3a56272a816ebf7d1a49b207b5e2",
                  "installmentLink": "https://sandbox-api.branvopay.com/boleto/v2/print/d1980d55fc05a3052655f61a6ac29e03"
                  "recurrence": {
                     "code": 9999,
                     "frequence": "monthly",
                     "lastCharge": "15/07/2020 12:05:00",
                     "nextCharge": "15/08/2020",
                     "dayNumber": "15"
                  }
              }
          ]
      }
  }

*********************************
Atualizar uma Recorrência
*********************************


:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/recurrent/{recurrenceCode}``

**Endpoint utilizado para atualizar uma Recorrência**

.. important::
   Os dados atualizados nesta requisição são os da recorrência, ou seja, as transações já efetuadas não serão afetadas. As próximas transações passarão a ser realizadas
   com estes dados. Através deste método, é possível estender a validade de uma recorrência, pausar uma recorrência, alterar a sua frequência e o seu valor.

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Header**                 | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | Account-Token*             | string          | | Token da conta na BranvoPay                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | frequence                  | string          | | Frequência de cobrança                                   | 
   |                            |                 | |                                                          |
   |                            |                 | | Valores aceitos:                                         |
   |                            |                 | | [monthly (mensal), bimonthly (cada 2 meses),             |
   |                            |                 | | quarterly (trimestral), semester (semestral),            |
   |                            |                 | | yearly (anual)]                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | amount                     | decimal         | | Valor da recorrência.                                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | finalDate                  | string          | | Data final da recorrência. Ela será executada            |
   |                            |                 | | automaticamente até esta data.                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | active                     | int             | | Recorrência ativa ou desativada/pausada                  |
   |                            |                 | | Valores aceitos: [0 (ativa), 1 (desativada)]             |
   +----------------------------+-----------------+------------------------------------------------------------+
   
.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   Nenhum dos parâmetros do corpo da requisição é obrigatório, porém é necessário informar pelo menos um deles para atualização.

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": {
         "active": bool,
         "amount": decimal,
         "code": int,
         "name": string,
         "createdAt": string,
         "lastCharge": string,
         "nextCharge": string,
         "finalDate": string,
         "frequence": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   **Body**                    **Valor enviado**
   frequence                   yearly
   finalDate                   2021-01-01
   amount                      110.00
   active                      0
   =========================== =================================

Header:

.. code:: javascript

   {
      "Account-Token":"*********************"
   }

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "active": false, //se a recorrência está ativa ou não
         "amount": "110.00",
         "code": 86, // código da recorrência, usado para atualização e consulta
         "name": "BRANVO PAY" //nome do cliente
         "createdAt": "06/12/2018 10:21:19", //data da criação da recorrência
         "lastCharge": "06/12/2018 10:21:19", //data da execução da última cobrança da recorrência
         "nextCharge": "01/01/2021", //data da próxima cobrança
         "finalDate": "01/01/2021", //data final da recorrêcia
         "frequence": "yearly"
      }
   }


*********************************
Consultar uma Recorrência
*********************************

:guilabel:`GET` ``https://api.branvopay.com/cartao/v2/recurrent/{recurrenceCode}``

**Endpoint utilizado para consultar os dados de uma Recorrência**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": {
         "active": bool,
         "amount": decimal,
         "code": int,
         "name": string,
         "createdAt": string,
         "lastCharge": string,
         "nextCharge": string,
         "finalDate": string,
         "frequence": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/recurrent/86

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "active": false, //se a recorrência está ativa ou não
         "amount": "110.00",
         "code": 86,
         "name": "BRANVO PAY" //nome do cliente
         "createdAt": "06/12/2018 10:21:19", //data da criação da recorrência
         "lastCharge": "06/12/2018 10:21:19", //data da execução da última transação da recorrência
         "nextCharge": "01/01/2021", //data final da recorrêcia
         "finalCate": "01/01/2021", //data final da recorrêcia
         "frequence": "yearly"
      }
   }